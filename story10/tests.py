from django.test import TestCase, Client
from django.urls import resolve
from .views import user_login, user_loginsuccess, user_logout, user_signup, index
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class UnitTestStory10(TestCase):
    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_function_login(self):
        found = resolve('/login')
        self.assertEqual(found.func, user_login)
    def test_function_login_success(self):
        found = resolve('/loginsuccess')
        self.assertEqual(found.func, user_loginsuccess)
    def test_function_logout(self):
        found = resolve('/logout')
        self.assertEqual(found.func, user_logout)
    def test_function_signup(self):
        found = resolve('/signup')
        self.assertEqual(found.func, user_signup)
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_landing_page_is_written(self):
        self.assertIsNotNone(index)

class FunctionalTestStory10(LiveServerTestCase):
    def setUp(self):
       chrome_options = Options()
       chrome_options.add_argument('--dns-prefetch-disable')
       chrome_options.add_argument('--no-sandbox')     
       chrome_options.add_argument('--disable-dev-shm-usage')           
       chrome_options.add_argument('--headless')
       chrome_options.add_argument('disable-gpu')
       self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
       super(FunctionalTestStory10, self).setUp()
    
    def tearDown(self):
       self.selenium.quit()
       super(FunctionalTestStory10, self).tearDown()

    def test_functional_story10(self):
        selenium = self.selenium
        self.selenium.get(self.live_server_url + '/')
        time.sleep(2)

        cektext = self.selenium.find_element_by_tag_name('body').text

        self.assertIn('by Johanes Simarmata', cektext)


        signupbutton_landing = selenium.find_element_by_name("tombol_signup_landing")
        signupbutton_landing.send_keys(Keys.RETURN)
        time.sleep(2)

        username = selenium.find_element_by_name("username")
        password = selenium.find_element_by_name("password")
        username.send_keys("Pepew")
        password.send_keys("isfun")
        time.sleep(3)
        submit_button = selenium.find_element_by_class_name("btn-success")
        submit = selenium.find_element_by_id("login-submit")
        time.sleep(2)
        submit_button.send_keys(Keys.RETURN)

        # self.selenium.get(self.live_server_url + '/')
        # time.sleep(2)
        
        # login_button_landing = selenium.find_element_by_class_name("btn")
        # login_button_landing.send_keys(Keys.RETURN)
        # box_username2 = selenium.find_element_by_id("username_box")
        # box_password = selenium.find_element_by_id("password_box")
        # button_submit = selenium.find_element_by_id("login-submit")
        
        # box_username2.send_keys("jo")
        # box_password.send_keys("1511")
        # button_submit.send_keys(Keys.RETURN)
        # checkteks = self.selenium.find_element_by_tag_name('body').text
        # self.assertIn("jo", checkteks)

        # logout = selenium.find_element_by_class_name("btn-danger")
        # logout.send_keys(Keys.RETURN)
        # time.sleep(2)


