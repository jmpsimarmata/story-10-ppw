from django.urls import path
from .views import index, user_login, user_loginsuccess, user_logout, user_signup

app_name = 'story10'
urlpatterns = [
    path('', index, name='index'),
    path('login', user_login, name='login'),
    path('loginsuccess', user_loginsuccess, name='loginsuccess'),
    path('logout', user_logout, name='logout'),
    path('signup', user_signup, name='signup')
]